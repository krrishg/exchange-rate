package com.krrishg.exchangerate.usecase;

import com.krrishg.exchangerate.external.ExchangeRateService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

class RetrieveExchangeRateUseCaseTest {
    @Autowired
    private ExchangeRateService service;

    @Test
    void testGetExchangeRate() {

        RestTemplate template = new RestTemplate();
        String url = String.format("http://api.nbp.pl/api/exchangerates/tables/%s/%s/?format=json", "A", "2021-12-22");
        System.out.println(template.getForObject(url, String.class));

//        SaveExchangeRateUseCase useCase = new SaveExchangeRateUseCase(new ExchangeRateService(repository));
//        useCase.execute();
//        RestTemplate template = new RestTemplate();
//        String forObject = template.getForObject("http://api.nbp.pl/api/exchangerates/rates/C/USD/2021-12-22?format=json", String.class);
//        System.out.println(forObject);
//        {"table":"C","currency":"dolar amerykański","code":"USD","rates":[{"no":"247/C/NBP/2021","effectiveDate":"2021-12-22","bid":4.0626,"ask":4.1446}]}

    }

}