package com.krrishg.exchangerate.usecase;

import com.krrishg.exchangerate.external.ExchangeRateService;
import com.krrishg.exchangerate.model.ExchangeRate;
import org.springframework.stereotype.Component;

@Component
public class RetrieveExchangeRateUseCase {
    private final ExchangeRateService service;

    public RetrieveExchangeRateUseCase(ExchangeRateService service) {
        this.service = service;
    }

    public Double execute(String currencyCode, String date) {
        ExchangeRate savedExchangeRate = service.getFromLocal(currencyCode, date);

        if (savedExchangeRate == null) {
            ExchangeRate exchangeRate = service.getRateFromApi(currencyCode, date);
            service.save(exchangeRate);
            savedExchangeRate = service.getFromLocal(currencyCode, date);
        }

        return savedExchangeRate.getRates().get(0).getAsk();
    }
}
