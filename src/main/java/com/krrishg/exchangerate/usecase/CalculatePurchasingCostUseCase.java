package com.krrishg.exchangerate.usecase;

import com.krrishg.exchangerate.external.ExchangeRateService;
import com.krrishg.exchangerate.model.ExchangeRate;
import com.krrishg.exchangerate.model.Rate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CalculatePurchasingCostUseCase {
    private final ExchangeRateService service;

    public CalculatePurchasingCostUseCase(ExchangeRateService service) {
        this.service = service;
    }

    public Double execute(List<String> currencyCodes, String publicationDate) {
        List<ExchangeRate> exchangeRates = service.getTableFromApi(publicationDate);
        return exchangeRates.get(0).getRates()
                .stream()
                .filter(rate -> currencyCodes.contains(rate.getCode()))
                .map(Rate::getMid)
                .reduce((double) 0, Double::sum);
    }
}
