package com.krrishg.exchangerate.database.repositories;

import com.krrishg.exchangerate.database.ExchangeRateEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExchangeRateRepository extends JpaRepository<ExchangeRateEntity, Long> {

    ExchangeRateEntity findByCodeAndPublicationDate(String code, String publicationDate);

}
