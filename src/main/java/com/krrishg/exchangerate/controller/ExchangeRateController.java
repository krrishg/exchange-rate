package com.krrishg.exchangerate.controller;

import com.krrishg.exchangerate.model.ExchangeRate;
import com.krrishg.exchangerate.model.ExchangeRateResponse;
import com.krrishg.exchangerate.model.MidExchangeRate;
import com.krrishg.exchangerate.model.Total;
import com.krrishg.exchangerate.usecase.CalculatePurchasingCostUseCase;
import com.krrishg.exchangerate.usecase.RetrieveExchangeRateUseCase;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/exchangerates")
public class ExchangeRateController {
    private final RetrieveExchangeRateUseCase retrieveExchangeRateUseCase;
    private final CalculatePurchasingCostUseCase calculatePurchasingCostUseCase;

    public ExchangeRateController(RetrieveExchangeRateUseCase retrieveExchangeRateUseCase,
                                  CalculatePurchasingCostUseCase calculatePurchasingCostUseCase) {
        this.retrieveExchangeRateUseCase = retrieveExchangeRateUseCase;
        this.calculatePurchasingCostUseCase = calculatePurchasingCostUseCase;
    }

    @GetMapping("/{currencyCode}/{publicationDate}")
    public ResponseEntity<ExchangeRateResponse> getExchangeRate(@PathVariable("currencyCode") String currencyCode,
                                                        @PathVariable("publicationDate") String publicationDate) {
        Double exchangeRate = retrieveExchangeRateUseCase.execute(currencyCode, publicationDate);
        ExchangeRateResponse total = new ExchangeRateResponse();
        total.setRate(exchangeRate);
        return ResponseEntity.ok(total);
    }

    @PostMapping()
    public ResponseEntity<Total> calculateTotalPurchasingCosts(@RequestBody MidExchangeRate body) {
        Double sum = calculatePurchasingCostUseCase.execute(body.getCurrencyCodes(), body.getPublicationDate());
        Total total = new Total();
        total.setTotal(sum);
        return ResponseEntity.ok(total);
    }
}
