package com.krrishg.exchangerate.model;

import java.util.List;

public class MidExchangeRate {
    private String publicationDate;
    private List<String> currencyCodes;

    public String getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(String publicationDate) {
        this.publicationDate = publicationDate;
    }

    public List<String> getCurrencyCodes() {
        return currencyCodes;
    }

    public void setCurrencyCodes(List<String> currencyCodes) {
        this.currencyCodes = currencyCodes;
    }
}
