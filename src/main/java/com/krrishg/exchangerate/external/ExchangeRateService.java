package com.krrishg.exchangerate.external;

import com.krrishg.exchangerate.database.ExchangeRateEntity;
import com.krrishg.exchangerate.database.repositories.ExchangeRateRepository;
import com.krrishg.exchangerate.model.ExchangeRate;
import com.krrishg.exchangerate.model.Rate;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class ExchangeRateService {
    private final ExchangeRateRepository repository;

    public ExchangeRateService(ExchangeRateRepository repository) {
        this.repository = repository;
    }

    public ExchangeRate getRateFromApi(String code, String date) {
        try {
            RestTemplate template = new RestTemplate();
            String url = String.format("http://api.nbp.pl/api/exchangerates/rates/C/%s/%s?format=json", code, date);
            return template.getForObject(url, ExchangeRate.class);
        } catch (Exception e) {
            throw new RuntimeException("cannot retrieve data");
        }
    }

    public List<ExchangeRate> getTableFromApi(String date) {
        try {
            RestTemplate template = new RestTemplate();
            String url = String.format("http://api.nbp.pl/api/exchangerates/tables/A/%s/?format=json", date);
            ExchangeRate[] rates = template.getForObject(url, ExchangeRate[].class);
            if (rates != null) {
                return Arrays.stream(rates).collect(Collectors.toList());
            }
            return Collections.emptyList();
        } catch (Exception e) {
            throw new RuntimeException("cannot retrieve data");
        }
    }

    public void save(ExchangeRate exchangeRate) {
        ExchangeRateEntity entity = toExchangeRate(exchangeRate);
        repository.save(entity);
    }

    public ExchangeRate getFromLocal(String code, String date) {
        ExchangeRateEntity entity = repository.findByCodeAndPublicationDate(code, date);
        return toExchangeRate(entity);
    }

    private ExchangeRate toExchangeRate(ExchangeRateEntity entity) {
        return Optional.ofNullable(entity).map(e -> {
            ExchangeRate exchangeRate = new ExchangeRate();
            exchangeRate.setCode(e.getCode());
            exchangeRate.setTable(e.getTable());
            exchangeRate.setCurrency(e.getCurrency());

            List<Rate> rates = getRates(e);
            exchangeRate.setRates(rates);
            return exchangeRate;
        }).orElse(null);

    }

    private List<Rate> getRates(ExchangeRateEntity entity) {
        List<Rate> rates = new ArrayList<>();

        Rate rate = new Rate();
        rate.setAsk(entity.getRate());
        rate.setEffectiveDate(entity.getPublicationDate());

        rates.add(rate);
        return rates;
    }

    private ExchangeRateEntity toExchangeRate(ExchangeRate exchangeRate) {
        ExchangeRateEntity entity = new ExchangeRateEntity();
        entity.setCode(exchangeRate.getCode());
        entity.setCurrency(exchangeRate.getCurrency());
        entity.setTable(exchangeRate.getTable());
        entity.setRate(exchangeRate.getRates().get(0).getAsk());
        entity.setPublicationDate(exchangeRate.getRates().get(0).getEffectiveDate());
        return entity;
    }
}
